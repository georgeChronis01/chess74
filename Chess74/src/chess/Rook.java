/**
 * @author George Chronis Emmanuel Ankrah
 */
package chess;

import java.util.ArrayList;
import java.util.HashMap;

// TODO: Auto-generated Javadoc
/**
 * The Class Rook.
 */
public class Rook extends Piece{
	
	/**
	 * Instantiates a new rook.
	 *
	 * @param name the name
	 * @param color the color
	 * @param row the row
	 * @param col the col
	 * @param owner the owner
	 * @param opponent the opponent
	 */
	public Rook(String name, String color, String row, String col, Player owner, Player opponent){
		this.setOwner(owner);
		this.setOpponent(opponent);
		this.setName(name);
		this.setColor(color);
		this.currentColumn=col;
		this.currentRow=row;
		this.setHasMoved(false);
//<<<<<<< HEAD
		//this.validMoves = new ArrayList<String>();
//=======
		this.validMoves = new ArrayList<String> ();
//>>>>>>> refs/remotes/origin/master
	}

	/* (non-Javadoc)
	 * @see chess.Piece#toString()
	 */
	@Override
	public String toString() {
		return this.getName();
	}

	/* (non-Javadoc)
	 * @see chess.Piece#calculateValidMoves()
	 */
	@Override
	public ArrayList<String> calculateValidMoves() {

		this.validMoves.clear();
		
		/*
		 * Rook can move horizontally or vertically through as many spaces as they want
		 * They CANNOT move diagonally at all
		 * 
		 */
		
		// column and row variables
		int y = Integer.parseInt(this.currentColumn);
		int x = Integer.parseInt(this.currentRow);
		
		
		// moving to the right  -- y constantly changes x remains the same
		for(int k = 1; k <= 7; k++){
			if((y + k) <= 7){
				if(Board.board[x][y+k] == null){
					this.validMoves.add("" + (x) + (y+k));
				}
				else if(Board.board[x][y+k] != null){
					if(Board.board[x][y+k].getColor().equalsIgnoreCase(this.getColor())){
						break;
					}
					else
						this.validMoves.add("" + (x) + (y+k));
				}
			}
		}
		
		// moving upward -- x changes y stays the same
		for(int j = 1; j <= 7; j++){
			if((x + j) <= 7){
				if(Board.board[x+j][y] == null){
					this.validMoves.add("" + (x+j) + (y));
				}
				else if(Board.board[x+j][y] != null){
					if(Board.board[x+j][y].getColor().equalsIgnoreCase(this.getColor())){
						break;
					}
					else if(Board.board[x+j][y].getColor().equalsIgnoreCase(this.getColor()) == false){
						this.validMoves.add("" + (x+j) + y);
						break;
					}
					else
						this.validMoves.add("" + (x+j) + (y));
				}
			}
		}
		
		// moving to the left - y changes x stays the same
				for(int k = 1; k <= 7; k++){
					if((y - k) >= 0){
						if(Board.board[x][y-k] == null){
							this.validMoves.add("" + (x) + (y-k));
						}
						else if(Board.board[x][y-k] != null){
							if(Board.board[x][y-k].getColor().equalsIgnoreCase(this.getColor())){
								break;
							}
							else if(Board.board[x][y-k].getColor().equalsIgnoreCase(this.getColor()) == false){
								this.validMoves.add("" + x + (y-k));
								break;
							}
							else
								this.validMoves.add("" + (x) + (y-k));
						}
					}
				}
				
				// moving downward vertically - x changes y stays the same
				for(int j = 1; j <= 7; j++){
					if((x - j) >= 0){
						if(Board.board[x-j][y] == null){
							this.validMoves.add("" + (x-j) + (y));
						}
						else if(Board.board[x-j][y] != null){
							if(Board.board[x-j][y].getColor().equalsIgnoreCase(this.getColor())){
								break;
							}
							else if(Board.board[x-j][y].getColor().equalsIgnoreCase(this.getColor()) == false){
								this.validMoves.add("" + (x-j) + y );
								break;
							}
							else
								this.validMoves.add("" + (x-j) + (y));
						}
					}
				}
				
				if(this.isProtectingKing()){
					validMoves.clear();
				}
				
				/*
				 * Castling
				 * Both king and rook need to be in original position 
				 */
				
				// left castling
				if(this.getHasMoved() == false){
					if(y == 0){
						if(Board.board[x][y+1] == null && Board.board[x][y+2] == null && Board.board[x][y+3] == null){
							if(Board.board[x][y+4] != null){
								if(Board.board[x][y+4].getHasMoved() == false){
									this.validMoves.add("" + x + (y+3));
								}
							}
						}
					}
				}
				
				//right castling
				if(this.getHasMoved() == false){
					if(y == 7){
						if(Board.board[x][y-1] == null & Board.board[x][y-2] == null){
							if(Board.board[x][y-3] != null){
								if(Board.board[x][y-3].getHasMoved() == false){
									this.validMoves.add("" + x + (y-3));
								}
							}
						}
					}
				}
				//moves will change if the owner is in check
				if(this.getOwner().getState().equalsIgnoreCase("check")){
					this.calculateMovesInCheck();
				}
				if(this.isProtectingKing()){
					validMoves.clear();
				}
		return this.validMoves;
	}
	
}
