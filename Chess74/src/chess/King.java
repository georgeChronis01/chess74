/**
 * @author George Chronis Emmanuel Ankrah
 */
package chess;

import java.util.ArrayList;
import java.util.HashMap;

// TODO: Auto-generated Javadoc
/**
 * The Class King.
 */
public class King extends Piece{
	
	/**
	 * Instantiates a new king.
	 *
	 * @param name the name
	 * @param color the color
	 * @param row the row
	 * @param col the col
	 * @param owner the owner
	 * @param opponent the opponent
	 */
	public King(String name, String color, String row, String col, Player owner, Player opponent){
		this.setOwner(owner);
		this.setOpponent(opponent);
		this.setName(name);
		this.setColor(color);
		this.currentColumn=col;
		this.currentRow=row;
		this.validMoves = new ArrayList<String> ();
		this.setHasMoved(false);
		//this.calculateValidMoves();
	}
	
	/* (non-Javadoc)
	 * @see chess.Piece#toString()
	 */
	@Override
	public String toString() {
		return this.getName();
	}

	/* (non-Javadoc)
	 * @see chess.Piece#calculateValidMoves()
	 */
	@Override
	public ArrayList<String> calculateValidMoves() {

		//will only call when a piece has moved, so should empty out current valid moves
		this.validMoves.clear();
		int y = Integer.parseInt(this.currentColumn);
		int x = Integer.parseInt(this.currentRow);
		//can only move one space in any direction if that space is  empty or not occupied by the same color and does not put it into check

		//check all eight directions
		
		// moving downward
		if((x+1) <= 7){
			if(Board.board[x+1][y] == null){
				if(this.getOpponent().getMoves().contains("" + (x+1) + y) == false){
					this.validMoves.add("" + (x+1) + y);
				}
			}
			else if(Board.board[x+1][y] != null){
				if(Board.board[x+1][y].getColor().equalsIgnoreCase(this.getColor()) == false){
					if(this.getOpponent().getMoves().contains("" + (x+1) + y) == false){
						this.validMoves.add("" + (x+1) + y);
					}
				}
			}
		}
		
		// moving upward
		if((x-1) >= 0){
			if(Board.board[x-1][y] == null){
				if(this.getOpponent().getMoves().contains("" + (x-1) + y) == false){
					this.validMoves.add("" + (x-1) + y);
				}
			}
			else if(Board.board[x-1][y] != null){
				if(Board.board[x-1][y].getColor().equalsIgnoreCase(this.getColor()) == false){
					if(this.getOpponent().getMoves().contains("" + (x-1) + y) == false){
						this.validMoves.add("" + (x-1) + y);
					}
					
				}
			}
		}
		
		// moving to the right
		if((y+1) <= 7){
			if(Board.board[x][y+1] == null){
				if(this.getOpponent().getMoves().contains("" + (x) + (y+1)) == false){
					this.validMoves.add("" + (x) + (y+1));
				}
			}
			else if(Board.board[x][y+1] != null){
				if(Board.board[x][y+1].getColor().equalsIgnoreCase(this.getColor()) == false){
					if(this.getOpponent().getMoves().contains("" + (x) + (y+1)) == false){
						this.validMoves.add("" + (x) + (y+1));
					}
				}
			}
		}
		
		// moving to the left
		if((y-1) >= 0){
			if(Board.board[x][y-1] == null){
				if(this.getOpponent().getMoves().contains("" + (x) + (y-1)) == false){
					this.validMoves.add("" + (x) + (y-1));
				}
			}
			else if(Board.board[x][y-1] == null){
				if(Board.board[x][y-1].getColor().equalsIgnoreCase(this.getColor()) == false){
					if(this.getOpponent().getMoves().contains("" + x + (y-1)) == false){
						this.validMoves.add("" + (x) + (y-1));
					}
				}
			}
		}
		
		// moving northeast -- x-1 and y+1
		if((x-1 >= 0) && (y+1 <= 7)){
			if(Board.board[x-1][y+1] == null){
				if(this.getOpponent().getMoves().contains("" + (x-1) + (y+1)) == false){
					this.validMoves.add("" + (x-1) + (y+1));
				}
			}
			else if(Board.board[x-1][y+1] != null){
				if(Board.board[x-1][y+1].getColor().equalsIgnoreCase(this.getColor()) == false){
					if(this.getOpponent().getMoves().contains("" + (x-1) + (y+1)) == false){
						this.validMoves.add("" + (x-1) + (y+1));
					}
				}
			}
		}
		
		// moving northwest -- x-1 and y-1
		if((x-1 >= 0) && (y-1 >= 0)){
			if(Board.board[x-1][y-1] == null){
				if(this.getOpponent().getMoves().contains("" + (x-1) + (y-1)) == false){
					this.validMoves.add("" + (x-1) + (y-1));
				}
			}
			else if(Board.board[x-1][y-1] != null){
				if(Board.board[x-1][y-1].getColor().equalsIgnoreCase(this.getColor()) == false){
					if(this.getOpponent().getMoves().contains("" + (x-1) + (y-1)) == false){
						this.validMoves.add("" + (x-1) + (y-1));
					}
				}
			}
		}
		
		// moving southwest -- x+1 and y-1
		if((x+1 <= 7) && (y-1 >= 0)){
			if(Board.board[x+1][y-1] == null){
				if(this.getOpponent().getMoves().contains("" + (x+1) + (y-1)) == false){
					this.validMoves.add("" + (x+1) + (y-1));
				}
			}
			else if(Board.board[x+1][y-1] != null){
				if(Board.board[x+1][y-1].getColor().equalsIgnoreCase(this.getColor()) == false){
					if(this.getOpponent().getMoves().contains("" + (x+1) + (y-1)) == false){
						this.validMoves.add("" + (x+1) + (y-1));
					}
				}
			}
		}
		
		// moving southeast -- x+1 and y+1
		if((x+1 <= 7) && (y+1 <= 7)){
			if(Board.board[x+1][y+1] == null){
				if(this.getOpponent().getMoves().contains("" + (x+1) + (y+1)) == false){
					this.validMoves.add("" + (x+1) + (y+1));
				}
			}
			else if(Board.board[x+1][y+1] != null){
				if(Board.board[x+1][y+1].getColor().equalsIgnoreCase(this.getColor()) == false){
					if(this.getOpponent().getMoves().contains("" + (x+1) + (y+1)) == false){
						this.validMoves.add("" + (x+1) + (y+1));
					}
				}
			}
		}
		
		/*
		 * Castling
		 * Both King and Rook need to be in original position in order for castling to work
		 */
		//you can't castle if you're in check
		if(this.getOwner().getState().equalsIgnoreCase("check")==false){
			// left rook castling
			if(this.getHasMoved() == false){
				if(Board.board[x][y-1] == null && Board.board[x][y-2] == null && Board.board[x][y-3] == null){
					if(Board.board[x][y-4] != null){
						if(Board.board[x][y-4].getHasMoved() == false&&this.getOpponent().getMoves().contains("" + (x) + (y-2)) == false){
							this.validMoves.add("" + x + (y-2));
						}
					}
				}
			}
			
			// right rook castling
			if(this.getHasMoved() == false){
				if(Board.board[x][y+1] == null && Board.board[x][y+2] == null){
					if(Board.board[x][y+3] != null){
						if(Board.board[x][y+3].getHasMoved() == false&&this.getOpponent().getMoves().contains("" + (x) + (y+2)) == false){
							this.validMoves.add("" + x + (y+2));
						}
					}
				}
			}
		}
//		ArrayList<String> saves = new ArrayList<String>();
//		if(this.getOwner().getState().equalsIgnoreCase("check")){
//			for(int  x1=0; x1<this.getOpponent().getPlayerPieces().size(); x1++ ){
//				//if piece contains the king's position
//				if(this.getOpponent().getPlayerPieces().get( x1).searchValidMoves(this.getOwner().getKingLocation()) ){
//					//this is the piece you either have to capture or get in the way of
//					Piece threat = this.getOpponent().getPlayerPieces().get( x1);
//					//1. capture
//					String capture = "" +threat.currentRow+ threat.currentColumn;
//					if(this.searchValidMoves(capture)){
//						//this move is valid....mark it?
//						//save it
//						saves.add(capture);
//					}
//				}
//			}
//		}if(!saves.isEmpty()){
//			validMoves.addAll(saves);
//		}
		return validMoves;
		
	}
	
}
