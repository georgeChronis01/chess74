/**
 * @author George Chronis Emmanuel Ankrah
 */
package chess;

import java.util.ArrayList;
import java.util.Scanner;

// TODO: Auto-generated Javadoc
/**
 * The Class Chess.
 */
public class Chess{
	
	/**
	 * Run chess.
	 */
	public static void runChess(){
		//initialize
		Player p1 = null;
		//WILL THE REASSIGNMENT WORK
		Player p2 = new Player("black", p1);
		p1= new Player("white", p2);
		p2.setOpponent(p1);
		Board.initializeBoard(p1, p2); 
	//	System.out.println(Board.board[7][4]);
		//one of them has to win for the game to end, or resign or draw or stalemate, you win by getting checkmate or 
		//getting rid of all 
		//the opposing  player's pieces, or

		
		//CHECKING INPUT WHAT IF SOURCE IS EMPTY???
		//WHAT IF SOURCE IS THE SAME AS DESTINATION????
		while(!p1.hasWon()&&!p2.hasWon()){
			boolean promote=false;
			//if no pieces, p1 lost
			if(p1.getPlayerPieces().isEmpty()||p1.getState().equalsIgnoreCase("lost")){
				p1.setState("lost");
				p2.setState("won");
				break;
			}
			//System.out.println(Board.board[7][4]);
			p2.getAllValidMoves();
			if(p1.isInCheck().equalsIgnoreCase("check")){
				System.out.println("Check");
				//something else should probably happen
				//isincheck will set the player's state to check and will evaluate valid moves accordingly
				//moves become limited to things that take player out of check, i.e. 
				//1. have to capture checking piece or 
				//2. get in the way of checking piece's path, or 
				//3. move king out of checking piece's path
				//how does a piece know what piece is putting the king in check?
				//by checking the opponents valid moves and returning the piece that has that move
				//and you do this by either finding all new valid moves, 
			}
			p1.getAllValidMoves();
			//have to do this because reevaluated valid moves for a piece could now be empty
			if(p1.isInCheckMate().equalsIgnoreCase("checkmate")){
				System.out.println("Checkmate");
				p2.setState("won");
				break;
				//end of game
			}
			//valid moves need to take into account being in check
			//p1.getAllValidMoves();
			//ALSO HAVE TO CHECK IF PLAYER HAS ANY PIECES LEFT
			//print board
			Board.printBoard(); 
			System.out.println();
			//player one goes first then player two
			
			//get the original position and position player wants to move to
			
			Scanner sc = new Scanner(System.in);
			System.out.print("White's move: ");
			//get entire line
			String input = sc.nextLine();
			String []inputs = input.split(" ");
			String source = inputs[0];
			String destination = inputs[1];
			String thirdInput = null;
			if(inputs.length==3){
				thirdInput=inputs[2];
			}
			
			//at this point the player can say resign or can ask other player to draw!!!!!!!!
			//check for that
			//System.out.println(source);
			if(source.equalsIgnoreCase("resign")){
				p1.setState("resign");
				p2.setState("won");
				break;
			}
			
			//SOURCE WILL NEVER EQUAL DRAW...asked after a play before next players turn
			if(thirdInput!=null&&thirdInput.equalsIgnoreCase("draw?")){
				//other player has to agree to draw, so get input from player two else the game continues on as normal
				String answer	= sc.next();
				if(answer.equalsIgnoreCase("draw")){
					p1.setState("draw");
					p2.setState("draw");
					break;
				}
				//else I think I just stole white's next move?
				else{
					//keep playing
				}
			}
			else if (thirdInput!=null){
				//third input is a promotion
				//check if source corresponds to a pawn
				if(Board.getPiece(source) instanceof Pawn){
					//check if pawn is on second to  last row, depending on whether it's black or white
					//but player one is always white, so if it's black then it's a bad input
					if(Board.getPiece(source).getColor().equals("white")&&Board.getPiece(source).currentRow.equals("1")){
						promote=true;
					}
					//black means bad input, but this will be checked next
				}
				//HAVE TO CHANGE THE INPUT METHOD FOR BLACK TOO
				
			}
//			System.out.println("");
			Piece piece =Board.getPiece(source);
			//it is possible there is no piece at designated source, invalid MOVE
			//also can't move other player's pieces
			//System.out.println(piece);
			while(piece==null||piece.getColor()!=p1.getColor()){
				System.out.println("Illegal move, try again");
				//System.out.println("White's move: ");
//				source=sc.next();
//				destination = sc.next();
				 input = sc.nextLine();
				 inputs = input.split(" ");
					source = inputs[0];
					 destination = inputs[1];
					 thirdInput = null;
					if(inputs.length==3){
						thirdInput=inputs[2];
					}
				piece =Board.getPiece(source);
				//moveTo= piece.searchValidMoves(destination);
				//I suppose there could be a case where this loop cannot exit?...
			}
			boolean moveTo= piece.searchValidMoves(destination);
			//if not a valid move reprompt
			//GOING TO ASSUME FOR NOW WILL ONLY ASK FOR A DRAW ONCE BEFORE THIS
			while(moveTo==false||piece.getColor()!=p1.getColor()){
				System.out.println("Illegal move, try again");
				//System.out.println("White's move: ");
				
				 input = sc.nextLine();
				 inputs = input.split(" ");
					source = inputs[0];
					 destination = inputs[1];
					 thirdInput = null;
					if(inputs.length==3){
						thirdInput=inputs[2];
					}
				
//				source=sc.next();
//				destination = sc.next();
				piece =Board.getPiece(source);
				moveTo= piece.searchValidMoves(destination);
				//I suppose there could be a case where this loop cannot exit?...
			}
			//else perform the move
			
			p1.moveTo(source, destination);
			piece.setHasMoved(true);
			if(promote){
				//if pawn is moved to last row player will promote it to it's choice of piece
				//throw out piece from space and from player's list
				//Piece target =Board.getPiece(destination);
				//System.out.println("pawn being promoted should be thrown out: "+piece );
				p1.getPlayerPieces().remove(piece);
				Board.removeFromBoard(destination);
				String x = destination.substring(1);
				String y =destination.substring(0,1);
				Piece prom=null;
				int row = Integer.parseInt(x);
				
				char column =  (char) (y.charAt(0) - '0');
				int realCol = (int)column;
				//have to subtract one from each b/c array zero indexed
				column--;
				//System.out.println(column+ " x " +  row);
				realCol-=49;
				row=(row-8)*-1;
				String finalRow = Integer.toString(row);
				String finalCol = Integer.toString(realCol);
				if(thirdInput.equals("N")){
					prom = new Knight("wN", "white",finalRow,finalCol,p1, p2);
				}
				else if(thirdInput.equals("K")){
					prom = new King("wK", "white",finalRow,finalCol, p1, p2);
				}
				else if(thirdInput.equals("Q")){
					System.out.println("row " + row+"column " +realCol );
					prom = new Queen("wQ", "white", finalRow,finalCol, p1, p2);
				}
				else if(thirdInput.equals("R")){
					prom = new Rook("wR", "white", finalRow,finalCol, p1, p2);
				}
				else{
					//bad input!!!!!
					//
					//
					////
					//
				}
				prom.setHasMoved(true);
				//System.out.println("PROMOTION");
				//create new piece based on player's input
				//add piece to player's list and put in correct board position
				Board.addToBoard(destination, prom);
				//Board.printBoard();
				p1.getPlayerPieces().add(prom);
				//reset boolean
				promote=false;
			}
			
			p1.getMoves().clear();
			p2.getMoves().clear();
			p1.getAllValidMoves();
			//if after a player's turn is over, the other player still has pawn's with double jump, it's now too late
			//turn all off
			for(int i =0; i<p2.getPlayerPieces().size();i++){
				if(p2.getPlayerPieces().get(i) instanceof Pawn){
					Piece x = p2.getPlayerPieces().get(i);
					Pawn pawn = (Pawn)x;
					if(pawn.doubleJump){
						pawn.doubleJump=false;
					}
				}
			}
			//no pieces left or they got the king
			if(p2.getPlayerPieces().isEmpty()||p2.getState().equalsIgnoreCase("lost")){
				p2.setState("lost");
				p1.setState("won");
				break;
			}
			//CHECK OR CHECKMATE??
			if(p2.isInCheck().equalsIgnoreCase("check")){
				System.out.println("Check");
				//something else should probably happen, player's state is set to check, which should be taken
				//into account when computing valid moves
			}
			p2.getAllValidMoves();
			if(p2.isInCheckMate().equalsIgnoreCase("checkmate")){
				System.out.println("Checkmate");
				p1.setState("won");
				break;
				//end of game
			}
			//else move on to player 2
			//reset moveTo
			moveTo=false;
			System.out.println();
			Board.printBoard();
			System.out.println();
			System.out.print("Black's move: ");
			 input = sc.nextLine();
			String []inputs2 = input.split(" ");
			 source = inputs2[0];
			 destination = inputs2[1];
			 thirdInput = null;
			if(inputs2.length==3){
				thirdInput=inputs2[2];
			}
			//get the original position and position player wants to move ot;
			 //source = sc.next();
			 if(source.equalsIgnoreCase("resign")){
					p1.setState("won");
					p2.setState("resign");
					break;
				}
//			destination = sc.next();
//			String draw2=sc.next();
			//SOURCE WILL NEVER EQUAL DRAW...asked after a play before next players turn
			if(thirdInput!=null&&thirdInput.equalsIgnoreCase("draw?")){
				//other player has to agree to draw, so get input from player two else the game continues on as normal
				String answer	= sc.next();
				if(answer.equalsIgnoreCase("draw")){
					p1.setState("draw");
					p2.setState("draw");
					break;
				}
				//else I think I just stole white's next move?
				else{
					//keep playing
				}
			}
			else if (thirdInput!=null){
				//third input is a promotion or a mistake
				//check if source corresponds to a pawn
				if(Board.getPiece(source) instanceof Pawn){
					//check if pawn is on second to  last row, depending on whether it's black or white
					if(Board.getPiece(source).getColor().equals("black")&&Board.getPiece(source).currentRow.equals("6")){
						promote=true;
					}
					//white is an error which will be caught
				}
				//HAVE TO CHANGE THE INPUT METHOD FOR BLACK TOO
			}
			 piece =Board.getPiece(source);
			 while(piece==null||piece.getColor()!=p2.getColor()){
					System.out.println("Illegal move, try again");
					//System.out.println("White's move: ");
					source=sc.next();
					destination = sc.next();
					piece =Board.getPiece(source);
					//moveTo= piece.searchValidMoves(destination);
					//I suppose there could be a case where this loop cannot exit?...
				}
			 moveTo= piece.searchValidMoves(destination);
			//if not a valid move reprompt
			 while(moveTo==false||piece.getColor()!=p2.getColor()){
					System.out.println("Illegal move, try again");
					//System.out.println("White's move: ");
					source=sc.next();
					destination = sc.next();
					piece =Board.getPiece(source);
//					if(sc.hasNext()){
//						
//					}
					moveTo= piece.searchValidMoves(destination);
					//I suppose there could be a case where this loop cannot exit?...
			}
			//else perform the move
			p2.moveTo(source, destination);
			piece.setHasMoved(true);
			if(promote){
				//if pawn is moved to last row player will promote it to it's choice of piece
				//throw out piece from space and from player's list
				//Piece target =Board.getPiece(destination);
				p2.getPlayerPieces().remove(piece);
				Board.removeFromBoard(destination);
				String x = source.substring(1);
				String y =source.substring(0,1);
				Piece prom=null;
				if(thirdInput.equals("N")){
					prom = new Knight("bN", "black", x,y, p2, p1);
				}
				else if(thirdInput.equals("K")){
					prom = new King("bK", "black", x,y, p2, p1);
				}
				else if(thirdInput.equals("Q")){
					prom = new Queen("bQ", "black", x,y, p2, p1);
				}
				else if(thirdInput.equals("R")){
					prom = new Rook("bR", "black", x,y, p2, p1);
				}
				else{
					//bad input!!!!!
					//
					//
					////
					//
				}
				
				//create new piece based on player's input
				//add piece to player's list and put in correct board position
				prom.setHasMoved(true);
				Board.addToBoard(destination, prom);
				p2.getPlayerPieces().add(prom);
				//if after a player's turn is over, the other player still has pawn's with double jump, it's now too late
				//turn all off
				for(int i =0; i<p1.getPlayerPieces().size();i++){
					if(p1.getPlayerPieces().get(i) instanceof Pawn){
						Piece z = p1.getPlayerPieces().get(i);
						Pawn pawn = (Pawn)z;
						if(pawn.doubleJump){
							pawn.doubleJump=false;
						}
					}
				}
			}
			System.out.println();
		}
		//either a player won, a player resigned, there was a mutual draw, or a mutual stalemate
		if(p1.getState().equalsIgnoreCase("Draw")){
			System.out.println("DRAW");
		}
		else if(p1.getState().equalsIgnoreCase("Stalemate")){
			System.out.println("STALEMATE");
		}
		//resigned or lost the same, so player 2 won
		else if(p1.getState().equalsIgnoreCase("Resigned")){
			System.out.println("PLAYER 2 WINS");
		}
		//else player 1 won 
		else if(p1.getState().equalsIgnoreCase("Won")){
			System.out.println("White Wins");
		}
		//else player 2 won
		else{
			System.out.println("Black Wins");
		}
	}
	

	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args){
		Chess.runChess();
	}
}