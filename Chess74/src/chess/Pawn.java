/**
 * @author George Chronis Emmanuel Ankrah
 */
package chess;

import java.util.ArrayList;


// TODO: Auto-generated Javadoc
/**
 * The Class Pawn.
 */
public class Pawn extends Piece{
	
	/** The double jump. */
	public boolean doubleJump;
	
	/** The en passant. */
	public boolean enPassant;
	
	/**
	 * Instantiates a new pawn.
	 *
	 * @param name the name
	 * @param color the color
	 * @param row the row
	 * @param col the col
	 * @param owner the owner
	 * @param opponent the opponent
	 */
	public Pawn(String name, String color, String row, String col, Player owner, Player opponent){
		this.setOwner(owner);
		this.setOpponent(opponent);
		this.setName(name);
		this.setColor(color);
		this.setHasMoved(false);
		this.enPassant=false;
		this.currentColumn=col;
		this.currentRow=row;
		this.doubleJump=false;
		this.validMoves = new ArrayList<String> ();
//		this.calculateValidMoves();
	}
	
	/* (non-Javadoc)
	 * @see chess.Piece#calculateValidMoves()
	 */
	@Override
	//actually this could just be a void return type, since we're just modifying a member of the piece
	public ArrayList<String>calculateValidMoves() {
		//will only call when a piece has moved, so should empty out current valid moves
		this.validMoves.clear();
		//1.
		//if pawn is on original space
		//it can double jump
		//lets assume whites always initialize on row 2 (actually row six in the matrix)
		int y = Integer.parseInt(this.currentColumn);
		int x = Integer.parseInt(this.currentRow);
		//if its white
		if(this.getName().contains("w")){
			//if on the correct row,
			if(this.currentRow.contains("6")){
				this.validMoves.add("4"+ y);
			}
		}
		//black on row seven (row one in matrix)
		else if(this.getName().contains("b")){
			//on starting row
			if(this.currentRow.contains("1")){
				this.validMoves.add("3"+ y);
				}
		}
		else{
			//error
			System.out.println("Pawn neither white nor black");
		}
		//2.
		// if its not on the edge of the board it can always move forward one space 
		//but if it's on the edge of the board, then it will be promoted, and the pawn will be thrown out,
		//so don't even have to consider this condition?
		//but a pawn cannot capture by moving forward, so the only constraint is that the
		//space in front of it is free
		//
		//white
		if(this.getName().contains("w")){
			if((x-1>=0)&&Board.board[x-1][y]==null){
				this.validMoves.add(""+(x-1)+y);
			}
		}
		//black
		if(this.getName().contains("b")){
			if((x+1<8)&&Board.board[x+1][y]==null){
				this.validMoves.add(""+(x+1)+y);
			}
		}
		//3.  EN PASSANT
		//or diagonal one space to the right  if 
		//a pawn double jumped onto this pawn's row and one space to the right
		//Board.getPiece(x,y) better than directly referencing the board?
		//if there is a case where saying x+1 would be outside hte board... you could just catch that here or there
		//for white 
		//can't go right if at column seven
		//have to do it right after they double jumped
		if(y!=7){
				Piece right = Board.board[x][y+1];
				Pawn rightPawn;
				if(this.getName().contains("w")){
					//Pawn leftPawn = (Pawn) Board.getPiece("");
					if(right instanceof Pawn){
						rightPawn = (Pawn)Board.board[x][y+1];
						if(right.getName().contains("bp")&&rightPawn.doubleJump==true){
							this.validMoves.add("" +(x-1)+ (y+1));
						}
					}
				}
				if(this.getName().contains("b")){
					if(right instanceof Pawn){
						rightPawn = (Pawn)Board.board[x][y+1];
						if(right.getName().contains("bp")&&rightPawn.doubleJump==true){
							this.validMoves.add("" +(x+1)+( y+1));
						}
					}
				}
			}
		//can't go left if on zeroth column
		if(y!=0){
			Pawn leftPawn;
			Piece left = Board.board[x][y-1];
			if(this.getName().contains("w")){
				// or diagonal one space to the left if a pawn double jumped onto this pawn's row and one space to the left
				if(left instanceof Pawn){
					leftPawn = (Pawn)Board.board[x][y-1];
					if(left.getName().contains("bp")&&leftPawn.doubleJump==true){
						this.validMoves.add(""+(x-1)+( y-1));
					}
				}
			}
			if(this.getName().contains("b")){
				// or diagonal one space to the left if a pawn double jumped onto this pawn's row and one space to the left
				if(left instanceof Pawn){
					leftPawn = (Pawn)Board.board[x][y-1];
					if(left.getName().contains("bp")&&leftPawn.doubleJump==true){
						this.validMoves.add(""+(x+1)+ (y-1));
					}
				}
			}
		}
		//4. or it can do a classic diagonal capture if there is a piece of the opposite color and doesn't go off the board
		if((x-1)>=0&&this.getName().contains("w")){
			if(y!=7&&Board.board[x-1][y+1]!=null&&Board.board[x-1][y+1].getColor().equalsIgnoreCase(this.getColor())==false){
				this.validMoves.add(""+(x-1)+( y+1));
			}
			//to the left
			if(y!=0&&Board.board[x-1][y-1]!=null&&Board.board[x-1][y-1].getColor().equalsIgnoreCase(this.getColor())==false){
				this.validMoves.add(""+(x-1)+( y-1));
			}
		}
		if((x+1)<8&&this.getName().contains("b")){
			if(y!=7&&Board.board[x+1][y+1]!=null&&Board.board[x+1][y+1].getColor().equalsIgnoreCase(this.getColor())==false){
				this.validMoves.add(""+(x+1)+ (y+1));
			}
			//to the left
			if(y!=0&&Board.board[x+1][y-1]!=null&&Board.board[x+1][y-1].getColor().equalsIgnoreCase(this.getColor())==false){
				this.validMoves.add(""+(x+1)+( y-1));
			}
		}
		ArrayList<String> saves = new ArrayList<String>();
		//filter moves if this player's king is in check
		//will this set off for checkmate too?
		if(this.getOwner().getState().equalsIgnoreCase("check")){
			//moves are only valid if this piece can
			//1.  capture checking piece or 
			//2. get in the way of checking piece's path, 
			//how does a piece know what piece is putting the king in check?
			//by checking the opponents valid moves and returning the piece that has that move threatening the king
			//for each piece
			for(int  x1=0; x1<this.getOpponent().getPlayerPieces().size(); x1++ ){
				//if piece contains the king's position
				if(this.getOpponent().getPlayerPieces().get( x1).searchValidMoves(this.getOwner().getKingLocation()) ){
					//this is the piece you either have to capture or get in the way of
					Piece threat = this.getOpponent().getPlayerPieces().get( x1);
					//1. capture
					String capture = "" +threat.currentRow+ threat.currentColumn;
					if(this.searchValidMoves(capture)){
						//this move is valid....mark it?
						//save it
						saves.add(capture);
					}
					//2. block , have to figure out threat's path if it has one, only valid for bish, rook, queen...
					//will either be one diagonal, horizontal, or vertical line
					//check using threat's position, will be in same row, column, or in a diagonal
					String kingRow =this.getOwner().getKingLocation().substring(0,1);
					String kingCol =  this.getOwner().getKingLocation().substring(1);
					int kingR = Integer.parseInt( kingRow);
					int kingC= Integer.parseInt( kingCol);
					int threatC =  Integer.parseInt( threat.currentColumn);
					int threatR =  Integer.parseInt( threat.currentRow);
					
					if(threat instanceof  Bishop){
						//if bishop is threatening the king
						//pawn can only interfere if the bishops path is in front of the pawn by one space
						//1. compute path from bishop to king, push all positions to array of positions and check against them, removing any move that isn't in 
						//the path array from the valid moves array
						//compute the path
						ArrayList<String> path = new ArrayList<String>();
						//if king's row and col are greater, the path is south east
						if(kingR>threatR&&kingC>threatC){
							//add all spaces from threat to king to path array
							//start at threat's position
							for(int k = 1; k <= 7; k++){ // make sure k is not out of bounds
								if((threatR + k) < kingR){ // x+k cannot be more than 8
									if((threatC+k) < kingC){ // y-k cannot be less than 0
											path.add("" + (threatR+k) + (threatC+k));
									}
								}
							}
						}
						//northwest
						else if(kingR<threatR&&kingC<threatC){
							for(int k = 1; k <= 7; k++){ // make sure k is not out of bounds
								if((threatR - k) > kingR){ // x+k cannot be more than 8
									if((threatC-k) > kingC){ // y-k cannot be less than 0
											path.add("" + (threatR-k) + (threatC-k));
									}
								}
							}
						}
						//row is greater, column is lesser, south east
						else if(kingR>threatR&&kingC<threatC){
							for(int k =1 ; k <= 7; k++){ // make sure k is not out of bounds
								if((threatR + k) < kingR){ // x+k cannot be more than 8
									if((threatC-k) > kingC){ // y-k cannot be less than 0
											path.add("" + (threatR+k) + (threatC-k));
									}
								}
							}
						}
						//
						else{
							for(int k = 1; k <= 7; k++){ // make sure k is not out of bounds
								if((threatR - k) > kingR){ // x+k cannot be more than 8
									if((threatC+k) < kingC){ // y-k cannot be less than 0
											path.add("" + (threatR-k) + (threatC+k));
									}
								}
							}
						}
						//for every validmovve, if exists in path keep, else delete
						for ( String item : this.validMoves) {
							//if valid move is in path, save it
						    if (path.contains(item)) {
						       saves.add(item);
						    }
						 }
					}
					else if(threat instanceof Rook){
						//if the rook is threatening king in a column, there is a clear from the rook to the king, so the pawn can't do anything because it needs a piece in the way to move to a new column
//							if(threat.currentColumn.equalsIgnoreCase(kingCol)&&this.getColor().equals("white")){
//								//nothing, can get rid of this
//							}
							//if the rook is in the same row as the king, then there is a clear path from the rook to hte king, but have to make sure pawn is actually between the two and not to the side
							if(threat.currentRow.equalsIgnoreCase(kingRow)){
								//if the pawn is white and below this row by one, it can move up and block if it is between the columns of the threat and the king
								int difference =Integer.parseInt( threat.currentRow)-Integer.parseInt(this.currentRow);
								//below means it is higher up on the row index
								//and the space is free! no space has to be free if pawn if actually between these two (has to be lesser than one and greater than the other
								if(this.getColor().equalsIgnoreCase("white")&&difference==-1&&(y<threatC&&y>kingC)||(y>threatC&&y<kingC)){
									saves.add(""+(x-1)+y);
								}
								//if the pawn is black and above this row by one is can move down and block, above means lower on matrix index, so difference will be positive
								if(this.getColor().equalsIgnoreCase("black")&&difference==1&&(y<threatC&&y>kingC)||(y>threatC&&y<kingC)){
									saves.add(""+(x+1)+y);
								}
							}
					}//end of rook
					//queen is just a combination of rook and bishop so just merge the code for both
					else if(threat instanceof  Queen){
						//rook stuff
						if(threat.currentRow.equalsIgnoreCase(kingRow)){
							//if the pawn is white and below this row by one, it can move up and block if it is between the columns of the threat and the king
							int difference =Integer.parseInt( threat.currentRow)-Integer.parseInt(this.currentRow);
							//below means it is higher up on the row index
							//and the space is free! no space has to be free if pawn if actually between these two (has to be lesser than one and greater than the other
							if(this.getColor().equalsIgnoreCase("white")&&difference==-1&&(y<threatC&&y>kingC)||(y>threatC&&y<kingC)){
								saves.add(""+(x-1)+y);
							}
							//if the pawn is black and above this row by one is can move down and block, above means lower on matrix index, so difference will be positive
							if(this.getColor().equalsIgnoreCase("black")&&difference==1&&(y<threatC&&y>kingC)||(y>threatC&&y<kingC)){
								saves.add(""+(x+1)+y);
							}
						}
						//bishop 
						ArrayList<String> path = new ArrayList<String>();
						//if king's row and col are greater, the path is south east
						if(kingR>threatR&&kingC>threatC){
							//add all spaces from threat to king to path array
							//start at threat's position
							for(int k = 1; k <= 7; k++){ // make sure k is not out of bounds
								if((threatR + k) < kingR){ // x+k cannot be more than 8
									if((threatC+k) < kingC){ // y-k cannot be less than 0
											path.add("" + (threatR+k) + (threatC+k));
									}
								}
							}
						}
						//northwest
						else if(kingR<threatR&&kingC<threatC){
							for(int k = 1; k <= 7; k++){ // make sure k is not out of bounds
								if((threatR - k) > kingR){ // x+k cannot be more than 8
									if((threatC-k) > kingC){ // y-k cannot be less than 0
											path.add("" + (threatR-k) + (threatC-k));
									}
								}
							}
						}
						//row is greater, column is lesser, south east
						else if(kingR>threatR&&kingC<threatC){
							for(int k =1 ; k <= 7; k++){ // make sure k is not out of bounds
								if((threatR + k) < kingR){ // x+k cannot be more than 8
									if((threatC-k) > kingC){ // y-k cannot be less than 0
											path.add("" + (threatR+k) + (threatC-k));
									}
								}
							}
						}
						//
						else{
							for(int k = 1; k <= 7; k++){ // make sure k is not out of bounds
								if((threatR - k) > kingR){ // x+k cannot be more than 8
									if((threatC+k) < kingC){ // y-k cannot be less than 0
											path.add("" + (threatR-k) + (threatC+k));
									}
								}
							}
						}
						//for every validmovve, if exists in path keep, else delete
						for ( String item : this.validMoves) {
							//if valid move is in path, save it
						    if (path.contains(item)) {
						       saves.add(item);
						    }
						 }
					
					}
					
				}
			}//end of for loop
			//get rid of all other moves and put in only the ones saved
			validMoves.clear();
			if(!saves.isEmpty()){
				validMoves.addAll(saves);
			}
		}//end of check procedure
		if(this.isProtectingKing()){
			validMoves.clear();
		}
		return this.validMoves;
	}
	
}
