/**
 * @author George Chronis Emmanuel Ankrah
 */
package chess;

import java.util.ArrayList;
import java.util.HashMap;

// TODO: Auto-generated Javadoc
/**
 * The Class Queen.
 */
public class Queen extends Piece{
	
	/**
	 * Instantiates a new queen.
	 *
	 * @param name the name
	 * @param color the color
	 * @param row the row
	 * @param col the col
	 * @param owner the owner
	 * @param opponent the opponent
	 */
	public Queen(String name, String color, String row, String col, Player owner, Player opponent){
		this.setOwner(owner);
		this.setOpponent(opponent);
		this.setName(name);
		this.setColor(color);
		this.currentColumn=col;
		this.currentRow=row;
		this.setHasMoved(false);
//<<<<<<< HEAD
		//this.validMoves = new ArrayList<String>();
//=======
		this.validMoves = new ArrayList<String> ();
//>>>>>>> refs/remotes/origin/master
	}

	/* (non-Javadoc)
	 * @see chess.Piece#toString()
	 */
	@Override
	public String toString() {
		return this.getName();
	}

	/* (non-Javadoc)
	 * @see chess.Piece#calculateValidMoves()
	 */
	@Override
	public ArrayList<String>calculateValidMoves() {
//<<<<<<< HEAD

		/*
		 * The Queen can move anywhere on the board. The most powerful piece
		 * Can move vertically, horizontally, and diagonally
		 * Similar to a mix of a rook and a bishop
		 */
		
		this.validMoves.clear();
		// variables for current column and row
		int y = Integer.parseInt(this.currentColumn);
		int x = Integer.parseInt(this.currentRow);
		
		// Rook movement
		
		// moving to the right  -- y constantly changes x remains the same
				for(int k = 1; k <= 7; k++){
					if((y + k) <= 7){
						if(Board.board[x][y+k] == null){
							this.validMoves.add("" + (x) + (y+k));
						}
						else if(Board.board[x][y+k] != null){
							if(Board.board[x][y+k].getColor().equalsIgnoreCase(this.getColor())){
								break;
							}
							else{
								this.validMoves.add("" + (x) + (y+k));
								break;
							}
						}
					}
				}
				
				// moving upward -- x changes y stays the same
				for(int j = 1; j <= 7; j++){
					if((x + j) <= 7){
						if(Board.board[x+j][y] == null){
							this.validMoves.add("" + (x+j) + (y));
						}
						else if(Board.board[x+j][y] != null){
							if(Board.board[x+j][y].getColor().equalsIgnoreCase(this.getColor())){
								break;
							}
							else if(Board.board[x+j][y].getColor().equalsIgnoreCase(this.getColor()) == false){
								this.validMoves.add("" + (x+j) + y);
								break;
							}
							else
								this.validMoves.add("" + (x+j) + (y));
						}
					}
				}
				
				// moving to the left - y changes x stays the same
						for(int k = 1; k <= 7; k++){
							if((y - k) >= 0){
								if(Board.board[x][y-k] == null){
									this.validMoves.add("" + (x) + (y-k));
								}
								else if(Board.board[x][y-k] != null){
									if(Board.board[x][y-k].getColor().equalsIgnoreCase(this.getColor())){
										break;
									}
									else if(Board.board[x][y-k].getColor().equalsIgnoreCase(this.getColor()) == false){
										this.validMoves.add("" + x + (y-k));
										break;
									}
									else
										this.validMoves.add("" + (x) + (y-k));
								}
							}
						}
						
						// moving downward vertically - x changes y stays the same
						for(int j = 1; j <= 7; j++){
							if((x - j) >= 0){
								if(Board.board[x-j][y] == null){
									this.validMoves.add("" + (x-j) + (y));
								}
								else if(Board.board[x-j][y] != null){
									if(Board.board[x-j][y].getColor().equalsIgnoreCase(this.getColor())){
										break;
									}
									else if(Board.board[x-j][y].getColor().equalsIgnoreCase(this.getColor()) == false){
										this.validMoves.add("" + (x-j) + y );
										break;
									}
									else
										this.validMoves.add("" + (x-j) + (y));
								}
							}
						}
				
			
			// Bishop Movement
		
						// moving northeast  
						for(int k = 1; k <= 7; k++){ // make sure k is not out of bounds
							if((x - k) >= 0){ // x+k cannot be more than 8
								if((y+k) <=7){ // y+k cannot be more than 8
									if(Board.board[x-k][y+k] == null){ // board space is empty, valid move
										this.validMoves.add("" + (x-k) + (y+k));
									}
									else if(Board.board[x-k][y+k] != null){ // board space is not empty
										if(Board.board[x-k][y+k].getColor().equalsIgnoreCase(this.getColor())){ //if same color piece, stop 
											break;
										}
										else if(Board.board[x-k][y+k].getColor().equalsIgnoreCase(this.getColor()) == false){
											this.validMoves.add("" + (x-k) + (y+k));
											break;
										}
										else
											this.validMoves.add("" + (x-k) + (y+k));
									}
								}
							}
						}
						
						// moving northwest
						for(int k = 1; k <= 7; k++){ // make sure k is not out of bounds
							if((x - k) >= 0){ // x+k cannot be more than 8
								if((y-k) >= 0){ // y-k cannot be less than 0
									if(Board.board[x-k][y-k] == null){ // board space is empty, valid move
										this.validMoves.add("" + (x-k) + (y-k));
									}
									else if(Board.board[x-k][y-k] != null){ // board space is not empty
										if(Board.board[x-k][y-k].getColor().equalsIgnoreCase(this.getColor())){ //if same color piece, stop 
											break;
										}
										else if(Board.board[x-k][y-k].getColor().equalsIgnoreCase(this.getColor()) == false){
											this.validMoves.add("" + (x-k) + (y-k));
											break;
										}
										else
											this.validMoves.add("" + (x-k) + (y-k));
									}
								}
							}
						}
						
						// moving southwest
						for(int k = 1; k <= 7; k++){ // make sure k is not out of bounds
							if((x + k) <= 7){ // x-k cannot be less than 0
								if((y-k) >= 0){ // y-k cannot be less than 0
									if(Board.board[x+k][y-k] == null){ // board space is empty, valid move
										this.validMoves.add("" + (x+k) + (y-k));
									}
									else if(Board.board[x+k][y-k] != null){ // board space is not empty
										if(Board.board[x+k][y-k].getColor().equalsIgnoreCase(this.getColor())){ //if same color piece, stop 
											break;
										}
										else if(Board.board[x+k][y-k].getColor().equalsIgnoreCase(this.getColor()) == false){
											this.validMoves.add("" + (x+k) + (y-k));
											break;
										}
										else
											this.validMoves.add("" + (x+k) + (y-k));
									}
								}
							}
						}
						
						// moving southeast
						for(int k = 1; k <= 7; k++){ // make sure k is not out of bounds
							if((x + k) <= 7){ // x+k cannot be more than 8
								if((y+k) <= 7){ // y-k cannot be less than 0
									if(Board.board[x+k][y+k] == null){ // board space is empty, valid move
										this.validMoves.add("" + (x+k) + (y+k));
									}
									else if(Board.board[x+k][y+k] != null){ // board space is not empty
										if(Board.board[x+k][y+k].getColor().equalsIgnoreCase(this.getColor())){ //if same color piece, stop 
											break;
										}
										else if(Board.board[x+k][y+k].getColor().equalsIgnoreCase(this.getColor()) == false){
											this.validMoves.add("" + (x+k) + (y+k));
											break;
										}
										else
											this.validMoves.add("" + (x+k) + (y+k));
									}
								}
							}
						}
						
						if(this.getOwner().getState().equalsIgnoreCase("check")){
							this.calculateMovesInCheck();
						}
						if(this.isProtectingKing()){
							validMoves.clear();
						}
						
		return this.validMoves;
	}
	
}
