/**@author George Chronis Emmanuel Ankrah
 * 
 */
package chess;


// TODO: Auto-generated Javadoc
/**
 * The Class Board.
 */
// test push
public class Board{
	
	/** The board. */
	public static Piece  [][] board;

	
	/**
	 * Gets the piece. using a string type position input expecting a letter and a number
	 *
	 * @param position the position
	 * @return the piece
	 */
	public static Piece getPiece(String position){
		//coords given in column row form
		String x = position.substring(1);
		String y =position.substring(0,1);
		//System.out.println(y+ " " + x);
		//x will be a letter, have to convert to number
		char column =  (char) (y.charAt(0) - '0');
		int realCol = (int)column;
		int row = Integer.parseInt(x);
		//have to subtract one from each b/c array zero indexed
		column--;
		//System.out.println(column+ " x " +  row);
		realCol-=49;
		//matrix is indexed upside down from the way board is printed
		//so subtract seven from row
		//System.out.println(""+realCol+row+Board.board[6][4]);
		row=(row-8)*-1;
		//System.out.println(""+realCol+row+Board.board[6][4]);
		return Board.board[row][realCol];
	}
	
	/**
	 * Prints the board. with pieces in correct places
	 */
	public static void printBoard(){
		//rows
		for(int i = 0; i<8; i++){
			//columns
		    for(int j = 0; j<8; j++){
		    	if(board[i][j]!=null){
		    		System.out.print(board[i][j]+" ");
		    	}
		    	//odd rows start with hashes
		    	else if(i%2!=0){
		    		//evens are hashes
		    		if(j%2==0){
		    			 System.out.print("## ");
		    		}
		    		else{//nine spaces
		    			 System.out.print("   ");
		    		}
			    }
		    	//even rows start with spaces
		    	else if(i%2==0){
		    		//evens are nine spaces
		    		if(j%2==0){
		    			 System.out.print("   ");
		    		}
		    		else{
		    			 System.out.print("## ");
		    		}
			    }
		    }//print number of row at end of inner loop
		    System.out.println(" "+ (8-i));
		}
		System.out.println(" a  b  c  d  e  f  g  h  ");
	}
	
	/**
	 * Initialize board. with pieces inside it
	 *
	 * @param p1 the p 1
	 * @param p2 the p 2
	 */
	public static void  initializeBoard(Player p1, Player p2){
		board = new Piece [][]{ 
			  {
				  p2.getPlayerPieces().get(0), p2.getPlayerPieces().get(1), 
				  p2.getPlayerPieces().get(2),  p2.getPlayerPieces().get(3),
				  p2.getPlayerPieces().get(4), p2.getPlayerPieces().get(5), 
				  p2.getPlayerPieces().get(6), p2.getPlayerPieces().get(7)
			  },
			  {
				  p2.getPlayerPieces().get(8), p2.getPlayerPieces().get(9),
				  p2.getPlayerPieces().get(10),  p2.getPlayerPieces().get(11),
				  p2.getPlayerPieces().get(12), p2.getPlayerPieces().get(13), 
				  p2.getPlayerPieces().get(14), p2.getPlayerPieces().get(15)
			  },
			  {null, null, null, null, null, null, null, null,},
			  {null, null, null, null, null, null, null, null,},
			  {null, null, null, null, null, null, null, null,},
			  {null, null, null, null, null, null, null, null,},
			  {
				  p1.getPlayerPieces().get(8), p1.getPlayerPieces().get(9), 
				  p1.getPlayerPieces().get(10), p1.getPlayerPieces().get(11), 
				  p1.getPlayerPieces().get(12), p1.getPlayerPieces().get(13), 
				  p1.getPlayerPieces().get(14), p1.getPlayerPieces().get(15)
				  },
			  {
				  p1.getPlayerPieces().get(0), p1.getPlayerPieces().get(1),
				  p1.getPlayerPieces().get(2), p1.getPlayerPieces().get(3), 
				  p1.getPlayerPieces().get(4), p1.getPlayerPieces().get(5), 
				  p1.getPlayerPieces().get(6), p1.getPlayerPieces().get(7)
			}
		};
	}

	/**
	 * Adds the piece  to board.
	 *
	 * @param destination the desired location of the piece
	 * @param piece the piece
	 * @return the piece
	 */
	public static Piece addToBoard(String destination, Piece piece){
		String x = destination.substring(1);
		String y =destination.substring(0,1);
		//System.out.println(y+ " " + x);
		//x will be a letter, have to convert to number
		char column =  (char) (y.charAt(0) - '0');
		int realCol = (int)column;
		int row = Integer.parseInt(x);
		//have to subtract one from each b/c array zero indexed
		column--;
		//System.out.println(column+ " x " +  row);
		realCol-=49;
		//matrix is indexed upside down from the way board is printed
		//so subtract seven from row
		//System.out.println(""+realCol+row+Board.board[6][4]);
		row=(row-8)*-1;
		//System.out.println(""+realCol+row+Board.board[6][4]);
		//put piece in board
		Board.board[row][realCol]=piece;
		return Board.board[row][realCol];
	}
	
	/**
	 * Removes the piece  from board.
	 *
	 * @param destination the current position of the piece
	 */
	public static void removeFromBoard(String destination){
		String x = destination.substring(1);
		String y =destination.substring(0,1);
		//System.out.println(y+ " " + x);
		//x will be a letter, have to convert to number
		char column =  (char) (y.charAt(0) - '0');
		int realCol = (int)column;
		int row = Integer.parseInt(x);
		//have to subtract one from each b/c array zero indexed
		column--;
		//System.out.println(column+ " x " +  row);
		realCol-=49;
		//matrix is indexed upside down from the way board is printed
		//so subtract seven from row
		//System.out.println(""+realCol+row+Board.board[6][4]);
		row=(row-8)*-1;
		//System.out.println(""+realCol+row+Board.board[6][4]);
		//put piece in board
		Board.board[row][realCol]=null;
	}
}