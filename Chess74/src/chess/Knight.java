/**
 * @author George Chronis Emmanuel Ankrah
 */
package chess;

import java.util.ArrayList;
import java.util.HashMap;

// TODO: Auto-generated Javadoc
/**
 * The Class Knight.
 */
public class Knight extends Piece{
	
	/**
	 * Instantiates a new knight.
	 *
	 * @param name the name
	 * @param color the color
	 * @param row the row
	 * @param col the col
	 * @param owner the owner
	 * @param opponent the opponent
	 */
	public Knight(String name, String color, String row, String col, Player owner, Player opponent){
		this.setOwner(owner);
		this.setOpponent(opponent);
		this.setName(name);
		this.setColor(color);
		this.currentColumn=col;
		this.currentRow=row;
		this.setHasMoved(false);
		this.validMoves = new ArrayList<String>();
	}

	/* (non-Javadoc)
	 * @see chess.Piece#toString()
	 */
	@Override
	public String toString() {
		return this.getName();
	}

	/* (non-Javadoc)
	 * @see chess.Piece#calculateValidMoves()
	 */
	@Override
	public ArrayList<String> calculateValidMoves() {

		
		this.validMoves.clear(); // clears valid moves
		//get current position of knight piece
		int y = Integer.parseInt(this.currentColumn); 
		int x = Integer.parseInt(this.currentRow);
		//switched x to be column
		
		// 1. Knights move in an L shape. 8 max different spots 
		// does not matter the color of a knight in terms of movement
		

		
		// 1. move 2 spaces up, one space to the right
		if((x-2 >= 0) && (y+1 <= 7) && Board.board[x-2][y+1] == null){
			this.validMoves.add("" + (x-2) + (y+1));
		}
		
		// 1. move 2 spaces up, one space to the right
		else if((x-2 >= 0) && (y+1 <= 7) && Board.board[x-2][y+1] != null){
					if(Board.board[x-2][y+1].getColor().equalsIgnoreCase(this.getColor()) == false){
						this.validMoves.add("" + (x-2) + (y+1));
					}
				}
		
		// 2. move 1 space up, two spaces to the right
		if((x-1 >= 0) && (y+2 <= 7) && Board.board[x-1][y+2] == null){
			this.validMoves.add("" + (x-1) + (y+2));
		}
		
		// 2. move 1 space up, two spaces to the right
		else if((x-1 >= 0) && (y+2 <= 7) && Board.board[x-1][y+2] != null){
			if(Board.board[x-1][y+2].getColor().equalsIgnoreCase(this.getColor()) == false){
				this.validMoves.add("" + (x-1) + (y+2));
			}
		}
		
		// 3. move 2 spaces up, 1 space to the left
		if((x-2 >= 0) && (y-1 >= 0) && Board.board[x-2][y-1] == null){
			this.validMoves.add("" + (x-2) + (y-1));
		}
		
		// 3. move 2 spaces up, 1 space to the left
		else if((x-2 >= 0) && (y-1 >= 0) && Board.board[x-2][y-1] != null){
			if(Board.board[x-2][y-1].getColor().equalsIgnoreCase(this.getColor()) == false){
				this.validMoves.add("" + (x-2) + (y-1));
			}
		}
		
		// 4. move 1 space up, 2 spaces to the left
		if((x-1 >= 0) && (y-2 >= 0) && Board.board[x-1][y-2] == null){
			this.validMoves.add("" + (x-1) + (y-2));
		}
		
		// 4. move 1 space up, 2 spaces to the left
		else if((x-1 >= 0) && (y-2 >= 0) && Board.board[x-1][y-2] != null){
			if(Board.board[x-1][y-2].getColor().equalsIgnoreCase(this.getColor()) == false){
				this.validMoves.add("" + (x-1) + (y-2));
			}
		}
		
		// 5. move 1 space down, 2 spaces to the right
		if((x+1 <= 7) && (y+2 <= 7) && Board.board[x+1][y+2] == null){
			this.validMoves.add("" + (x+1) + (y+2));
		}
		// 5. move 1 space down, 2 spaces to the right
		else if((x+1 <= 7) && (y+2 <= 7) && Board.board[x+1][y+2] != null){
			if(Board.board[x+1][y+2].getColor().equalsIgnoreCase(this.getColor()) == false){
				this.validMoves.add("" + (x+1) + (y+2));
			}
		}
		
		
		// 6. move 2 spaces down, 1 space to the right
		if((x+2 <= 7) && (y+1 <= 7) && Board.board[x+2][y+1] == null){
			this.validMoves.add("" + (x+2) + (y+1));
		}
		
		// 6. move 2 spaces down, 1 space to the right
		else if((x+2 <= 7) && (y+1 <= 7) && Board.board[x+2][y+1] != null){
			if(Board.board[x+2][y+1].getColor().equalsIgnoreCase(this.getColor()) == false){
				this.validMoves.add("" + (x+2) + (y+1));
			}
		}
		
		// 7. move one space down, 2 spaces to the left
		if((x+1 <= 7) && (y-2 >= 0) && Board.board[x+1][y-2] == null){
			this.validMoves.add("" + (x+1) + (y-2));
		}
		
		// 7. move one space down, 2 spaces to the left
		else if((x+1 <= 7) && (y-2 >= 0) && Board.board[x+1][y-2] != null){
			if(Board.board[x+1][y-2].getColor().endsWith(this.getColor()) == false){
				this.validMoves.add("" + (x+1) + (y-2));
			}
		}
		
		// 8. move 2 spaces down, 1 space left
		if((x+2 <= 7) && (y-1 >= 0) && Board.board[x+2][y-1] == null){
			this.validMoves.add("" + (x+2) + (y-1));
		}
		
		// 8. move 2 spaces down, 1 space left
		else if((x+2 <= 7) && (y-1 >= 0) && Board.board[x+2][y-1] != null){
			if(Board.board[x+2][y-1].getColor().equalsIgnoreCase(this.getColor()) == false){
				this.validMoves.add("" + (x+2) + (y-1));
			}
		}
		
		/*
		 * Now need to cover moves where there are pieces available to be taken
		 */
		//moves will change if the owner is in check
		if(this.getOwner().getState().equalsIgnoreCase("check")){
			this.calculateMovesInCheck();
		}
		if(this.isProtectingKing()){
			validMoves.clear();
		}
	return this.validMoves;
}
}
