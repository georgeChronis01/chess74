/**
 * @author George Chronis Emmanuel Ankrah
 */
package chess;

import java.util.ArrayList;
import java.util.HashMap;

// TODO: Auto-generated Javadoc
/**
 * The Class Bishop.
 */
public class Bishop extends Piece{
	
	/**
	 * Instantiates a new bishop.
	 *
	 * @param name the name
	 * @param color the color
	 * @param row the row
	 * @param col the column
	 * @param owner the owner
	 * @param opponent the opponent
	 */
	public Bishop(String name, String color, String row, String col, Player owner, Player opponent){
		this.setOwner(owner);
		this.setOpponent(opponent);
		this.setName(name);
		this.setColor(color);
		this.currentColumn=col;
		this.currentRow=row;
		this.setHasMoved(false);

		//this.validMoves = new ArrayList<String>();

		this.validMoves = new ArrayList<String> ();
	}

	/* (non-Javadoc)
	 * @see chess.Piece#toString()
	 */
	@Override
	public String toString() {
		return this.getName();
	}

	
	/* (non-Javadoc)
	 * @see chess.Piece#calculateValidMoves()
	 */
	public ArrayList<String> calculateValidMoves() {
		/*
		 * The Bishop can move in ONLY diagonals
		 * column and row must change at the same time 
		 */
		
		this.validMoves.clear();
		
		// set variables for column and row
		int y = Integer.parseInt(this.currentColumn);
		int x = Integer.parseInt(this.currentRow);
		
		// moving northeast  
		for(int k = 1; k <= 7; k++){ // make sure k is not out of bounds
			if((x - k) >= 0){ // x+k cannot be more than 8
				if((y+k) <=7){ // y+k cannot be more than 8
					if(Board.board[x-k][y+k] == null){ // board space is empty, valid move
						this.validMoves.add("" + (x-k) + (y+k));
					}
					else if(Board.board[x-k][y+k] != null){ // board space is not empty
						if(Board.board[x-k][y+k].getColor().equalsIgnoreCase(this.getColor())){ //if same color piece, stop 
							break;
						}
						else if(Board.board[x-k][y+k].getColor().equalsIgnoreCase(this.getColor()) == false){
							this.validMoves.add("" + (x-k) + (y+k));
							break;
						}
						else
							this.validMoves.add("" + (x-k) + (y+k));
					}
				}
			}
		}
		
		// moving northwest
		for(int k = 1; k <= 7; k++){ // make sure k is not out of bounds
			if((x - k) >= 0){ // x+k cannot be more than 8
				if((y-k) >= 0){ // y-k cannot be less than 0
					if(Board.board[x-k][y-k] == null){ // board space is empty, valid move
						this.validMoves.add("" + (x-k) + (y-k));
					}
					else if(Board.board[x-k][y-k] != null){ // board space is not empty
						if(Board.board[x-k][y-k].getColor().equalsIgnoreCase(this.getColor())){ //if same color piece, stop 
							break;
						}
						else if(Board.board[x-k][y-k].getColor().equalsIgnoreCase(this.getColor()) == false){
							this.validMoves.add("" + (x-k) + (y-k));
							break;
						}
						else
							this.validMoves.add("" + (x-k) + (y-k));
					}
				}
			}
		}
		
		// moving southwest
		for(int k = 1; k <= 7; k++){ // make sure k is not out of bounds
			if((x + k) <= 7){ // x-k cannot be less than 0
				if((y-k) >= 0){ // y-k cannot be less than 0
					if(Board.board[x+k][y-k] == null){ // board space is empty, valid move
						this.validMoves.add("" + (x+k) + (y-k));
					}
					else if(Board.board[x+k][y-k] != null){ // board space is not empty
						if(Board.board[x+k][y-k].getColor().equalsIgnoreCase(this.getColor())){ //if same color piece, stop 
							break;
						}
						else if(Board.board[x+k][y-k].getColor().equalsIgnoreCase(this.getColor()) == false){
							this.validMoves.add("" + (x+k) + (y-k));
							break;
						}
						else
							this.validMoves.add("" + (x+k) + (y-k));
					}
				}
			}
		}
		
		// moving southeast
		for(int k = 1; k <= 7; k++){ // make sure k is not out of bounds
			if((x + k) <= 7){ // x+k cannot be more than 8
				if((y+k) <= 7){ // y-k cannot be less than 0
					if(Board.board[x+k][y+k] == null){ // board space is empty, valid move
						this.validMoves.add("" + (x+k) + (y+k));
					}
					else if(Board.board[x+k][y+k] != null){ // board space is not empty
						if(Board.board[x+k][y+k].getColor().equalsIgnoreCase(this.getColor())){ //if same color piece, stop 
							break;
						}
						else if(Board.board[x+k][y+k].getColor().equalsIgnoreCase(this.getColor()) == false){
							this.validMoves.add("" + (x+k) + (y+k));
							break;
						}
						else
							this.validMoves.add("" + (x+k) + (y+k));
					}
				}
			}
		}
		//moves will change if the owner is in check
		if(this.getOwner().getState().equalsIgnoreCase("check")){
			this.calculateMovesInCheck();
		}
		if(this.isProtectingKing()){
			validMoves.clear();
		}
		return this.validMoves;

	}
	
}
