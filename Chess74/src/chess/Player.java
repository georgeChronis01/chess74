/**
 * @author George Chronis Emmanuel Ankrah
 */
package chess;

import java.util.ArrayList;

// TODO: Auto-generated Javadoc
/**
 * The Class Player.
 */
class Player{
	
	/** The opponent. */
	private Player opponent;
	
	/** The king location. */
	private String kingLocation;
	
	/** The color. */
	private String color;
	
	/** The pieces. */
	private ArrayList<Piece> pieces;
	
	/** The state. */
	private String state;
	
	/** The moves of the player for every piece. */
	private ArrayList<String> moves;
	
	/**
	 * Gets all the moves of a player.
	 *
	 * @return the moves
	 */
	public ArrayList<String> getMoves() {
		return moves;
	}
	
	/**
	 * Sets the moves of a player.
	 *
	 * @param moves the new moves
	 */
	public void setMoves(ArrayList<String> moves) {
		this.moves = moves;
	}
	
	/**
	 * Instantiates a new player with pieces inside its list
	 *
	 * @param color the color
	 * @param opponent the opponent
	 */
	public Player(String color, Player opponent){
		this.moves = new ArrayList<String> ();
		this.setState("playing");
		this.setColor(color);
		this.opponent=opponent;
		//if branch for black or white
		if(color.equalsIgnoreCase("black")){
			setPlayerPieces(new ArrayList<Piece>());
			getPlayerPieces().add(new Rook("bR", "black", "0", "0", this, this.getOpponent())); 
			getPlayerPieces().add(new Knight("bN", "black", "0", "1", this, this.getOpponent())); 
			getPlayerPieces().add(new Bishop("bB", "black", "0", "2", this, this.getOpponent())); 
			getPlayerPieces().add(new Queen("bQ", "black", "0", "3", this, this.getOpponent())); 
			this.kingLocation="04";
			getPlayerPieces().add(new King("bK", "black", "0", "4", this, this.getOpponent())); 
			getPlayerPieces().add(new Bishop("bB", "black", "0", "5", this, this.getOpponent())); 
			getPlayerPieces().add(new Knight("bN", "black", "0", "6", this, this.getOpponent())); 
			getPlayerPieces().add(new Rook("bR", "black", "0", "7", this, this.getOpponent())); 
			getPlayerPieces().add(new Pawn("bp", "black", "1", "0", this, this.getOpponent())); 
			getPlayerPieces().add(new Pawn("bp", "black", "1", "1", this, this.getOpponent())); 
			getPlayerPieces().add(new Pawn("bp", "black", "1", "2", this, this.getOpponent())); 
			getPlayerPieces().add(new Pawn("bp", "black", "1", "3", this, this.getOpponent())); 
			getPlayerPieces().add(new Pawn("bp", "black", "1", "4", this, this.getOpponent())); 
			getPlayerPieces().add(new Pawn("bp", "black", "1", "5", this, this.getOpponent())); 
			getPlayerPieces().add(new Pawn("bp", "black", "1", "6", this, this.getOpponent())); 
			getPlayerPieces().add(new Pawn("bp", "black", "1", "7", this, this.getOpponent())); 
		}
		else if(color.equalsIgnoreCase("white")){
			setPlayerPieces(new ArrayList<Piece>());
			getPlayerPieces().add(new Rook("wR", "white", "7", "0", this, this.getOpponent())); 
			getPlayerPieces().add(new Knight("wN", "white","7", "1", this, this.getOpponent())); 
			getPlayerPieces().add(new Bishop("wB", "white","7", "2", this, this.getOpponent())); 
			getPlayerPieces().add(new Queen("wQ", "white","7", "3", this, this.getOpponent())); 
			getPlayerPieces().add(new King("wK", "white","7", "4", this, this.getOpponent())); 
			this.kingLocation="74";
			getPlayerPieces().add(new Bishop("wB", "white","7", "5", this, this.getOpponent())); 
			getPlayerPieces().add(new Knight("wN", "white","7", "6", this, this.getOpponent())); 
			getPlayerPieces().add(new Rook("wR", "white","7", "7", this, this.getOpponent())); 
			getPlayerPieces().add(new Pawn("wp", "white", "6", "0", this, this.getOpponent())); 
			getPlayerPieces().add(new Pawn("wp", "white", "6", "1", this, this.getOpponent())); 
			getPlayerPieces().add(new Pawn("wp", "white", "6", "2", this, this.getOpponent())); 
			getPlayerPieces().add(new Pawn("wp", "white", "6", "3", this, this.getOpponent())); 
			getPlayerPieces().add(new Pawn("wp", "white", "6", "4", this, this.getOpponent())); 
			getPlayerPieces().add(new Pawn("wp", "white", "6", "5", this, this.getOpponent())); 
			getPlayerPieces().add(new Pawn("wp", "white", "6", "6", this, this.getOpponent())); 
			getPlayerPieces().add(new Pawn("wp", "white", "6", "7", this, this.getOpponent())); 
		}
	}
	
	/**
	 * Gets the player pieces list.
	 *
	 * @return the player pieces
	 */
	public ArrayList<Piece> getPlayerPieces() {
		return pieces;
	}
	
	/**
	 * Sets the player pieces.
	 *
	 * 
	 */
	public void setPlayerPieces(ArrayList<Piece> pieces) {
		this.pieces = pieces;
	}
	
	/**
	 * Checks for won.
	 *
	 * @return true, if successful
	 */
	public boolean hasWon(){
		return false;
	}
	//if a a player's king's position is in the list of possible moves for the other player, this player is in check
	/**
	 * Checks if player  is in check.
	 *
	 * @return the string
	 */
	//if a player is in check and has no moves itself, checkmate
	public String isInCheck(){
		//for every piece, calculate valid moves and put into moves array for player, or 
		if(this.opponent.moves.contains(this.getKingLocation())){
			this.setState("check");
			return "check";
		}
		return "no";
	}
	
	/**
	 * Checks if player  is in check mate.
	 *
	 * @return the string state
	 */
	public String isInCheckMate(){
		//for every piece, calculate valid moves and put into moves array for player, or 
		if(this.opponent.moves.contains(this.getKingLocation())&&this.getMoves().size()==0){
			this.setState("checkmate");
			return "checkmate";
		}
		return "no";
	}
	
	/**
	 * Calculates all the valid moves for a player
	 *
	 * @return the all valid moves
	 */
	//gets all valid moves for every piece for player, this is used for finding stalemates, and checking whether the king is in check or can move to a certain spot
	public ArrayList<String> getAllValidMoves(){
		for(int i =0;i<this.getPlayerPieces().size();i++){
			moves.addAll(this.getPlayerPieces().get(i).calculateValidMoves());
		}
		return moves;
	}
	
	/**
	 * Move a player's piece from its location to its destination Takes care of en passant, castling, and captures
	 *
	 * @param source the source
	 * @param destination the destination
	 * @return the piece
	 */
	public Piece  moveTo(String source,  String destination){
		Piece piece =Board.getPiece(source);
		//convert position to ints
		String x = source.substring(1);
		String y =source.substring(0,1);
		//System.out.println(y+ " " + x);
		//x will be a letter, have to convert to number
		char column =  (char) (y.charAt(0) - '0');
		int realCol = (int)column;
		int row = Integer.parseInt(x);
		//have to subtract one from each b/c array zero indexed
		column--;
		//System.out.println(column+ " x " +  row);
		realCol-=49;
		//matrix is indexed upside down from the way board is printed
		//so subtract seven from row
		
		row=(row-8)*-1;
		//System.out.println("row: "+row+realCol);
		
		
		String b =destination.substring(0,1);
		String a =destination.substring(1);
		//x will be a letter, have to convert to number
		int destCol =  b.charAt(0) - '0';
		int finalCol = (int)destCol;
		int finalRow = Integer.parseInt(a);
		column--;
		//System.out.println(column+ " x " +  row);
		finalCol-=49;
		//matrix is indexed upside down from the way board is printed
		//so subtract seven from row
		finalRow=(finalRow-8)*-1;
		//finalRow-=1;
		//System.out.println("test");
		//and put into new position , obvs have to do before set original space to null
//		System.out.println("row "+row + "  col"+ realCol);
//		System.out.println("finalrow "+finalRow + " final col"+ finalCol);
		//THROW OUT PIECE AT DESTINATION IF IT'S THERE
		if(Board.board[finalRow][finalCol]!=null){
			//get that piece and delete it from the player's piece list
			Piece target =Board.getPiece(destination);
			if(target instanceof King){
				this.setState("won");
				this.getOpponent().setState("lost");
			}
			target.getOwner().getPlayerPieces().remove(target);
		}
		//put piece in destination
			Board.board[finalRow][finalCol]=piece;
				//System.out.println("test");
				
				//actually remove the piece from its current position, replace with null, 
			Board.board[row][realCol]=null; 
				
		//and change the piece's current row and column
		//want to keep things in numbers from here because every calculate validMoves will use the row and column directly 
		//as matrix indices, so take the the numbers parsed and turn them back into integers 
		//and subtract one from them because matrices and zero indexed
		String trueCol = ""+finalCol;
		String trueRow = ""+finalRow;
		piece.currentColumn=trueCol;
		piece.currentRow=trueRow;
		//if a king, remember to rest it's location
		if(piece.getName().contains("K")){
			this.setKingLocation(""+trueRow+trueCol);
			//check for castling
			//difference between source and destination columns will be two
			int diff = finalCol-realCol;
			if(diff==2){
				//move the rook on the right to the right of king's original position
				//Board.board[finalRow][finalCol] = piece;
				//get rook
				char destinationLetter = trueCol.charAt(0);
				destinationLetter+=1;
				System.out.println(destinationLetter);
				String destLet = ""+destinationLetter+ trueRow;
				System.out.println(destLet);
				//Piece Rook = Board.getPiece(""  + destLet+trueRow);
				Piece rook= Board.board[finalRow][finalCol+1];
				//put rook in correct spot
				Board.board[finalRow][finalCol - 1] = rook;
				//Board.board[row][realCol] = null;
				Board.board[row][finalCol + 1] = null;
			}
			if(diff==-2){
				//move the rook on the left to the left of king's original position
				//Piece Rook = Board.getPiece(""  + (finalCol - 2)+ finalRow);
				//Board.board[finalRow][finalCol] = piece;
				Piece rook= Board.board[finalRow][finalCol-2];
				Board.board[finalRow][finalCol + 1] = rook;
				//Board.board[row][realCol] = null;
				Board.board[row][finalCol - 2] = null;
			}
		}
		if(piece instanceof Pawn){
			//en passant
			int diff = finalRow-row;
			if(Math.abs(diff)==2){
				((Pawn) piece).doubleJump=true;
			}
			else{
				((Pawn) piece).doubleJump=false;
			}
			//throw out the piece that is either below in the same column(for white) or above in the same column(for black)
			if(piece.getColor().equalsIgnoreCase("white")){
				//remove from board and remove from opponent's piece list
				Piece target =Board.board[finalRow+1][finalCol];
				Pawn pawn = (Pawn)target;
				//if piece at the right spot double jumped and is of a different color
				if(pawn!=null&&pawn.doubleJump&&pawn.getColor().equals(piece)==false){
					target.getOwner().getPlayerPieces().remove(target);
					Board.board[finalRow+1][finalCol]=null;
				}
				
			}
			//black
			else{
				Piece target =Board.board[finalRow-1][finalCol];
				Pawn pawn = (Pawn)target;
				//if piece at the right spot double jumped and is of a different color
				if(pawn!=null&&pawn.doubleJump&&pawn.getColor().equals(piece)==false){
					target.getOwner().getPlayerPieces().remove(target);
					Board.board[finalRow-1][finalCol]=null;
				}
//				target.getOwner().getPlayerPieces().remove(target);
//				Board.board[finalRow-1][finalCol]=null;
			}
			
		}
		return piece;
	}
	
	/**
	 * Gets the state.
	 *
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	
	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(String state) {
		this.state = state;
	}
	
	/**
	 * Gets the color.
	 *
	 * @return the color
	 */
	public String getColor() {
		return color;
	}
	
	/**
	 * Sets the color.
	 *
	 * @param color the new color
	 */
	public void setColor(String color) {
		this.color = color;
	}
	
	/**
	 * Gets the king location.
	 *
	 * @return the king location
	 */
	public String getKingLocation() {
		return kingLocation;
	}
	
	/**
	 * Sets the king location.
	 *
	 * @param kingLocation the new king location
	 */
	public void setKingLocation(String kingLocation) {
		String location ;
		//search 
		this.kingLocation = kingLocation;
	}
	
	/**
	 * Gets the opponent.
	 *
	 * @return the opponent
	 */
	public Player getOpponent() {
		return opponent;
	}
	
	/**
	 * Sets the opponent.
	 *
	 * @param opponent the new opponent
	 */
	public void setOpponent(Player opponent) {
		this.opponent = opponent;
		//go through player's pieces and assign to make sure
		for(int i =0;i<16;i++){
			this.getPlayerPieces().get(i).setOpponent(opponent);
		}
	}
}