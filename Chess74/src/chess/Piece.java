package chess;

import java.util.ArrayList;


// TODO: Auto-generated Javadoc
/**
 * The Class Piece.
 */
public abstract class Piece{
	
	/** The owner. */
	private Player owner;
	
	/** The opponent. */
	private Player opponent;
	
	/** The name. */
	private String name;
	
	/** The color. */
	private String color;
	
	/** The has moved. */
	private boolean hasMoved;
	
	/** The k. */
	//maybe?
	private final String k="King";
	
	/** The a. */
	public final char a = 0;
	
	/** The current position. */
	public String currentPosition;
	
	/** The current row. */
	public String currentRow;
	
	/** The current column. */
	public String currentColumn;
	
	/** The original position. */
	public String originalPosition;
	
	/** The starting row. */
	public String startingRow;
	
	/** The valid moves. */
	ArrayList<String>validMoves;
	
	/**
	 * Calculate valid moves.
	 *
	 * @return the array list
	 */
	public abstract ArrayList<String> calculateValidMoves();
	
	/**
	 * Calculate moves while player in check.
	 */
	public void calculateMovesInCheck(){
		int y = Integer.parseInt(this.currentColumn);
		int x = Integer.parseInt(this.currentRow);
		ArrayList<String> saves = new ArrayList<String>();
		//filter moves if this player's king is in check
		//will this set off for checkmate too?
		//if(this.getOwner().getState().equalsIgnoreCase("check")){
			//moves are only valid if this piece can
			//1.  capture checking piece or 
			//2. get in the way of checking piece's path, 
			//how does a piece know what piece is putting the king in check?
			//by checking the opponents valid moves and returning the piece that has that move threatening the king
			//for each piece
			for(int  x1=0; x1<this.getOpponent().getPlayerPieces().size(); x1++ ){
				//if piece contains the king's position
				if(this.getOpponent().getPlayerPieces().get( x1).searchValidMoves(this.getOwner().getKingLocation()) ){
					//this is the piece you either have to capture or get in the way of
					Piece threat = this.getOpponent().getPlayerPieces().get( x1);
					//1. capture
					String capture = "" +threat.currentRow+ threat.currentColumn;
					if(this.searchValidMoves(capture)){
						//this move is valid....mark it?
						//save it
						saves.add(capture);
					}
					//2. block , have to figure out threat's path if it has one, only valid for bish, rook, queen...
					//will either be one diagonal, horizontal, or vertical line
					//check using threat's position, will be in same row, column, or in a diagonal
					String kingRow =this.getOwner().getKingLocation().substring(0,1);
					String kingCol =  this.getOwner().getKingLocation().substring(1);
					int kingR = Integer.parseInt( kingRow);
					int kingC= Integer.parseInt( kingCol);
					int threatC =  Integer.parseInt( threat.currentColumn);
					int threatR =  Integer.parseInt( threat.currentRow);
					
					//2.5 if this is called on the king in check, it can't block 
					//so 
					
					if(!(this instanceof King)){
						//sooo business as usual
					
					
					
					if(threat instanceof  Bishop){
						//if bishop is threatening the king
						//pawn can only interfere if the bishops path is in front of the pawn by one space
						//1. compute path from bishop to king, push all positions to array of positions and check against them, removing any move that isn't in 
						//the path array from the valid moves array
						//compute the path
						ArrayList<String> path = new ArrayList<String>();
						//if king's row and col are greater, the path is south east
						if(kingR>threatR&&kingC>threatC){
							//add all spaces from threat to king to path array
							//start at threat's position
							for(int k = 1; k <= 7; k++){ // make sure k is not out of bounds
								if((threatR + k) < kingR){ // x+k cannot be more than 8
									if((threatC+k) < kingC){ // y-k cannot be less than 0
											path.add("" + (threatR+k) + (threatC+k));
									}
								}
							}
						}
						//northwest
						else if(kingR<threatR&&kingC<threatC){
							for(int k = 1; k <= 7; k++){ // make sure k is not out of bounds
								if((threatR - k) > kingR){ // x+k cannot be more than 8
									if((threatC-k) > kingC){ // y-k cannot be less than 0
											path.add("" + (threatR-k) + (threatC-k));
									}
								}
							}
						}
						//row is greater, column is lesser, south east
						else if(kingR>threatR&&kingC<threatC){
							for(int k =1 ; k <= 7; k++){ // make sure k is not out of bounds
								if((threatR + k) < kingR){ // x+k cannot be more than 8
									if((threatC-k) > kingC){ // y-k cannot be less than 0
											path.add("" + (threatR+k) + (threatC-k));
									}
								}
							}
						}
						//
						else{
							for(int k = 1; k <= 7; k++){ // make sure k is not out of bounds
								if((threatR - k) > kingR){ // x+k cannot be more than 8
									if((threatC+k) < kingC){ // y-k cannot be less than 0
											path.add("" + (threatR-k) + (threatC+k));
									}
								}
							}
						}
						//for every validmovve, if exists in path keep, else delete
						for ( String item : this.validMoves) {
							//if valid move is in path, save it
						    if (path.contains(item)) {
						       saves.add(item);
						    }
						 }
					}
					else if(threat instanceof Rook){
						ArrayList<String> path = new ArrayList<String>();
						//if the rook is threatening king in a column, there is a clear from the rook to the king, so the pawn can't do anything because it needs a piece in the way to move to a new column
						//1. check column paths	
						if(threat.currentColumn.equalsIgnoreCase(kingCol)){
								//going up
								if(threatC<kingC){
									for(int j = 1; j <= 7; j++){
										if((threatR + j) < kingR){
											if(Board.board[threatR+j][threatC] == null){
												path.add("" + (threatR+j) + (threatC));
											}
										}
									}
								}
								//going down
								else{
									for(int j = 1; j <= 7; j++){
										if((threatR - j) > kingR){
											if(Board.board[threatR-j][y] == null){
												this.validMoves.add("" + (threatR-j) + (threatC));
											}
										}
									}
								}
							}
						//if the rook is in the same row as the king, then there is a clear path from the rook to hte king, but have to make sure pawn is actually between the two and not to the side
						if(threat.currentRow.equalsIgnoreCase(kingRow)){
							//going left
							if(threatR>kingR){
								for(int k = 1; k <= 7; k++){
									if((threatC - k) > kingC){
										if(Board.board[threatR][threatC-k] == null){
											this.validMoves.add("" + (threatR) + (threatC-k));
										}
									}
								}
							}
							//going right
							if(threatR<kingR){
								for(int k = 1; k <= 7; k++){
									if((threatC + k) < kingC){
										if(Board.board[threatR][threatC+k] == null){
											this.validMoves.add("" + (threatR) + (threatC+k));
										}
									}
								}
							}
						}
						//for every validmove, if exists in path keep, else delete
						for ( String item : this.validMoves) {
							//if valid move is in path, save it
						    if (path.contains(item)) {
						       saves.add(item);
						    }
						 }
					}//end of rook
					//queen is just a combination of rook and bishop so just merge the code for both
					else if(threat instanceof  Queen){
						//rook stuff
						ArrayList<String> path = new ArrayList<String>();
						//if the rook is threatening king in a column, there is a clear from the rook to the king, so the pawn can't do anything because it needs a piece in the way to move to a new column
						//1. check column paths	
						if(threat.currentColumn.equalsIgnoreCase(kingCol)){
								//going up
								if(threatC<kingC){
									for(int j = 1; j <= 7; j++){
										if((threatR + j) < kingR){
											if(Board.board[threatR+j][threatC] == null){
												path.add("" + (threatR+j) + (threatC));
											}
										}
									}
								}
								//going down
								else{
									for(int j = 1; j <= 7; j++){
										if((threatR - j) > kingR){
											if(Board.board[threatR-j][y] == null){
												this.validMoves.add("" + (threatR-j) + (threatC));
											}
										}
									}
								}
							}
						//if the rook is in the same row as the king, then there is a clear path from the rook to hte king, but have to make sure pawn is actually between the two and not to the side
						if(threat.currentRow.equalsIgnoreCase(kingRow)){
							//going left
							if(threatR>kingR){
								for(int k = 1; k <= 7; k++){
									if((threatC - k) > kingC){
										if(Board.board[threatR][threatC-k] == null){
											this.validMoves.add("" + (threatR) + (threatC-k));
										}
									}
								}
							}
							//going right
							if(threatR<kingR){
								for(int k = 1; k <= 7; k++){
									if((threatC + k) < kingC){
										if(Board.board[threatR][threatC+k] == null){
											this.validMoves.add("" + (threatR) + (threatC+k));
										}
									}
								}
							}
						}
					
						//bishop 
						//ArrayList<String> path = new ArrayList<String>();
						//if king's row and col are greater, the path is south east
						if(kingR>threatR&&kingC>threatC){
							//add all spaces from threat to king to path array
							//start at threat's position
							for(int k = 1; k <= 7; k++){ // make sure k is not out of bounds
								if((threatR + k) < kingR){ // x+k cannot be more than 8
									if((threatC+k) < kingC){ // y-k cannot be less than 0
											path.add("" + (threatR+k) + (threatC+k));
									}
								}
							}
						}
						//northwest
						else if(kingR<threatR&&kingC<threatC){
							for(int k = 1; k <= 7; k++){ // make sure k is not out of bounds
								if((threatR - k) > kingR){ // x+k cannot be more than 8
									if((threatC-k) > kingC){ // y-k cannot be less than 0
											path.add("" + (threatR-k) + (threatC-k));
									}
								}
							}
						}
						//row is greater, column is lesser, south east
						else if(kingR>threatR&&kingC<threatC){
							for(int k =1 ; k <= 7; k++){ // make sure k is not out of bounds
								if((threatR + k) < kingR){ // x+k cannot be more than 8
									if((threatC-k) > kingC){ // y-k cannot be less than 0
											path.add("" + (threatR+k) + (threatC-k));
									}
								}
							}
						}
						//
						else{
							for(int k = 1; k <= 7; k++){ // make sure k is not out of bounds
								if((threatR - k) > kingR){ // x+k cannot be more than 8
									if((threatC+k) < kingC){ // y-k cannot be less than 0
											path.add("" + (threatR-k) + (threatC+k));
									}
								}
							}
						}
						//for every validmove, if exists in path keep, else delete
						for ( String item : this.validMoves) {
							//if valid move is in path, save it
						    if (path.contains(item)) {
						       saves.add(item);
						    }
						 }
					
					}
					
				}
				}	// end of not a king
			}//end of for loop
			//get rid of all other moves and put in only the ones saved
			validMoves.clear();
			if(!saves.isEmpty()){
				validMoves.addAll(saves);
			}
		}//end of check procedure
	
	/**
	 * Search for move in piece's valid moves.
	 *
	 * @param destination the destination
	 * @return true, if successful
	 */
	public boolean searchValidMoves(String destination){
		//valid moves are saved as xy/row column, have to convert destination into that
		//format
		String x = destination.substring(1);
		String y =destination.substring(0,1);
		//System.out.println(y+ " " + x);
		//x will be a letter, have to convert to number
		char column =  (char) (y.charAt(0) - '0');
		int realCol = (int)column;
		int row = Integer.parseInt(x);
		//have to subtract one from each b/c array zero indexed
		column--;
		//System.out.println(column+ " x " +  row);
		realCol-=49;
		//matrix is indexed upside down from the way board is printed
		//so subtract seven from row
		
		row=(row-8)*-1;
		//System.out.println(""+realCol+row);
		String destMod = ""+row+realCol;
		for ( String item : this.validMoves) {
		    if (item.equalsIgnoreCase(destMod)) {
		       return true;
		    }
		 }
		return false;
	}
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return this.getName();
	}

	/**
	 * Gets the color.
	 *
	 * @return the color
	 */
	public String getColor() {
		return color;
	}

	/**
	 * Sets the color.
	 *
	 * @param color the new color
	 */
	public void setColor(String color) {
		this.color = color;
	}
	
	/**
	 * Checks if is protecting king.
	 *
	 * @return true, if is protecting king
	 */
	//EVERY PIECE MUST CHECK THIS AT THE END OF calculateValidMoves, if it is protecting king, empty the list
	public boolean isProtectingKing(){
		//how do you check if it's protecting a king?
		// must be in the way of the threatening piece and a king
		//OR
		//if this piece is in a diagonal row or column with the king and with a queen, bish, rook
		//which means either the row or column has to be the same for hor or vert OR row and column have to be the same distance away
		String row =this.getOwner().getKingLocation().substring(0,1);
		String col =this.getOwner().getKingLocation().substring(1);
		//check if king is on same row and if threat is on same row
		//to check if threat is on same row iether check if your position is in the other player's moves and for which piece that is true
		//or just scan the opposite way from the king and if an opposite color rook or queen is there you'r done, i'll do it this way
		if(this.currentRow.equals(row)){
			//with clear path to you and a clear path from you to the king
			//1. check for clear path from you to king, have to check this traversign over columns
			int distance = Integer.parseInt(col) - Integer.parseInt(this.currentColumn);
			int kingCol= Integer.parseInt(col);
			int thisCol= Integer.parseInt(this.currentColumn);
			int thisRow= Integer.parseInt(this.currentRow);
			//king is to the right
			if(distance>0){
				//for every space between you and king there must be no piece on the board there
				//if you''re at the edge of the board you  can't have the king to the right....but the positive distance suggests that the king is already to the right sooo. 
				//if(thisCol<7){
					for(int i =thisCol+1;i<kingCol;i++){
						//if the path is not clear, you're not protecting the king, WAIT BUT YOU HAVE TO CHECK IF YOU HIT THE KING????? OR NO bc only iterate the distance between the two
						if(Board.board[thisRow][i]!=null){
							return false;
						}
					}
				//}
				//path all clear, now check path from you to threatening piece, and you're done
				//going to the left so decrement to zero
				for(int i =thisCol-1;i>=0;i--){
					//when you get to the first isntance of a queen or rook, you're protecting the king, otherwise you're not
					//if theres a piece thats not a queen or rook, return false
					if(Board.board[thisRow][i]!=null&&(!(Board.board[thisRow][i] instanceof Queen) &&!(Board.board[thisRow][i] instanceof Rook ))){
						return false;
					}
					//this condition does not get triggered if a space in the row is empty
				}
				//if you got through the whole row without returning, it means you didn't find a queen or rook on that row, so its false?
				return true;
			}
			//king is to the left
			if(distance<0){
				for(int i =thisCol-1;i>kingCol;i--){
					//if the path is not clear, you're not protecting the king, bc only iterate the distance between the two
					if(Board.board[thisRow][i]!=null){
						return false;
					}
				}
			//}
			//path all clear, now check path from you to threatening piece, and you're done
			//going to the left so decrement to zero
			for(int i =thisCol+1;i<8;i++){
				//if the path is not clear, check color, if not opposite and a queen or rook you're not protecting the king
				if(Board.board[thisRow][i]!=null&&(!(Board.board[thisRow][i] instanceof Queen) &&!(Board.board[thisRow][i] instanceof Rook ))){
					return false;
				}
			}
			}
		}
		//column check if threat is on same row 
		else if (this.currentColumn.equals(col)){
			//with clear path to you and a clear path from you to the king
			//1. check you to king
			int distance = Integer.parseInt(row) - Integer.parseInt(this.currentRow);
			int kingRow= Integer.parseInt(row);
			int thisCol= Integer.parseInt(this.currentColumn);
			int thisRow= Integer.parseInt(this.currentRow);
			//king is to the right
			if(distance>0){
				//for every space between you and king there must be no piece on the board there
				//if you''re at the edge of the board you  can't have the king to the right....but the positive distance suggests that the king is already to the right sooo. 
				//if(thisCol<7){
					for(int i =thisRow+1;i<kingRow;i++){
						//if the path is not clear, you're not protecting the king, WAIT BUT YOU HAVE TO CHECK IF YOU HIT THE KING????? 
						//OR NO bc only iterate the distance between the two
						if(Board.board[i][thisCol]!=null){
							return false;
						}
					}
				//}
				//path all clear, now check path from you to threatening piece, and you're done
				//going to the left so decrement to zero
				for(int i =thisRow-1;i>=0;i--){
					//when you get to the first isntance of a queen or rook, you're protecting the king, otherwise you're not
					if(Board.board[i][thisCol]!=null&&(!(Board.board[i][thisCol] instanceof Queen) &&!(Board.board[i][i=thisCol] instanceof Rook ))){
						return false;
					}
				}
				return true;
			}
			//king is to the left
			if(distance<0){
				for(int i =thisRow-1;i>kingRow;i--){
					//if the path is not clear, you're not protecting the king, bc only iterate the distance between the two
					if(Board.board[i][thisCol]!=null){
						return false;
					}
				}
			//}
			//path all clear, now check path from you to threatening piece, and you're done
			//going to the left so decrement to zero
			for(int i =thisCol+1;i<8;i++){
				//if the path is not clear, check color, if not opposite and a queen or rook you're not protecting the king
				if(Board.board[i][thisCol]!=null&&(!(Board.board[i][thisCol] instanceof Queen) &&!(Board.board[i][thisCol] instanceof Rook ))){
					return false;
				}
			}
		}
		}
		//diagonal
		else{
			//clear path from threat to you and from you to king
		}
		
		
		
		
		

			
		
		return false;
	}
	
	/**
	 * Gets the opponent.
	 *
	 * @return the opponent
	 */
	public Player getOpponent() {
		return opponent;
	}
	
	/**
	 * Sets the opponent.
	 *
	 * @param opponent the new opponent
	 */
	public void setOpponent(Player opponent) {
		this.opponent = opponent;
	}
	
	/**
	 * Gets the owner.
	 *
	 * @return the owner
	 */
	public Player getOwner() {
		return owner;
	}
	
	/**
	 * Sets the owner.
	 *
	 * @param owner the new owner
	 */
	public void setOwner(Player owner) {
		this.owner = owner;
	}
	
	/**
	 * Gets the checks for moved.
	 *
	 * @return the checks for moved
	 */
	public boolean getHasMoved() {
		return hasMoved;
	}
	
	/**
	 * Sets the checks for moved.
	 *
	 * @param hasMoved the new checks for moved
	 */
	public void setHasMoved(boolean hasMoved) {
		this.hasMoved = hasMoved;
	}
}
